import React, { ReactNode, useMemo } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// import { Web3ReactProvider } from '@web3-react/core';
// import { Provider } from 'react-redux';
// import store from 'state';
// import { getLibrary } from 'utils/web3React';
import ListsUpdater from './state/lists/updater'
import MulticallUpdater from './state/multicall/updater'
import TransactionUpdater from './state/transactions/updater'
import Providers from './Providers';
// import useActiveWeb3React from 'hooks/useActiveWeb3React';

function Updaters() {
  return (
    <>
      <ListsUpdater />
      <TransactionUpdater />
      <MulticallUpdater />
    </>
  )
}

// function Blocklist({ children }: { children: ReactNode }) {
//   const { account } = useActiveWeb3React()
//   const blocked: boolean = useMemo(() => Boolean(account && BLOCKED_ADDRESSES.indexOf(account) !== -1), [account])
//   if (blocked) {
//     return <div>Blocked address</div>
//   }
//   return <>{children}</>
// }

// Sentry.init({
//   dsn: 'https://ed98e16b9d704c22bef92d24bdd5f3b7@o1092725.ingest.sentry.io/6111410',
//   integrations: [
//     new Integrations.BrowserTracing(),
//     new Sentry.Integrations.Breadcrumbs({
//       console: process.env.NODE_ENV === 'production',
//     }),
//   ],
//   environment: process.env.NODE_ENV,
//   // Set tracesSampleRate to 1.0 to capture 100%
//   // of transactions for performance monitoring.
//   // We recommend adjusting this value in production
//   tracesSampleRate: 0.1,
//   beforeSend(event, hint) {
//     const error = hint?.originalException
//     if (error && isUserRejected(error)) {
//       return null
//     }
//     return event
//   },
//   ignoreErrors: [
//     'User denied transaction signature',
//     'Non-Error promise rejection captured',
//     'User rejected the transaction',
//     'cancelled',
//   ],
// })

ReactDOM.render(
  <React.StrictMode>
    <Providers>
      <Updaters />
      <App />
    </Providers>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
