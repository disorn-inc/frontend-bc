import { nanoid } from "@reduxjs/toolkit";
import { useCallback } from "react";
import { TokenList } from "@uniswap/token-lists";
import { useDispatch } from "react-redux";
import { AppDispatch } from "../state";
import { fetchTokenList } from "../state/lists/actions";
import resolveENSContentHash from "../utils/ENS/resolveENSContentHash";
import useActiveWeb3React from "./useActiveWeb3React";
import useWeb3Provider from "./useActiveWeb3React";
import { ChainId } from "@disorn/golfswap-testnet-sdk";

function useFetchListCallback(): (
  listUrl: string,
  sendDispatch?: boolean
) => Promise<TokenList> {
  const { library } = useWeb3Provider();
  const { chainId } = useActiveWeb3React();
  const dispatch = useDispatch<AppDispatch>();

  const ensResolver = useCallback(
    (ensName: string) => {
      if (chainId !== ChainId.MAINNET) {
        throw new Error("Could not construct mainnet ENS resolver");
      }
      return resolveENSContentHash(ensName, library);
    },
    [chainId, library]
  );

  // note: prevent dispatch if using for list search or unsupported list
  return useCallback(
    async (listUrl: string, sendDispatch = true) => {
      const requestId = nanoid();
      if (sendDispatch) {
        dispatch(fetchTokenList.pending({ requestId, url: listUrl }));
      }
      // lazy load avj and token list schema
      const getTokenList = (await import("../utils/getTokenList")).default;
      return getTokenList(listUrl, ensResolver)
        .then((tokenList) => {
          if (sendDispatch) {
            dispatch(
              fetchTokenList.fulfilled({ url: listUrl, tokenList, requestId })
            );
          }
          return tokenList;
        })
        .catch((error) => {
          console.error(`Failed to get list at url ${listUrl}`, error);
          if (sendDispatch) {
            dispatch(
              fetchTokenList.rejected({
                url: listUrl,
                requestId,
                errorMessage: error.message,
              })
            );
          }
          throw error;
        });
    },
    [dispatch, ensResolver]
  );
}

export default useFetchListCallback;
