import { useMemo } from "react";
import { getContract, getProviderOrSigner } from "../utils";
import {
  getBep20Contract,
  getCakeContract,
  getMasterchefContract,
} from "../utils/contractHelpers";
import useActiveWeb3React from "./useActiveWeb3React";
import { Contract } from "@ethersproject/contracts";
import {
  Bep20,
  EnsPublicResolver,
  Erc20Bytes32,
  Multicall,
  Weth,
} from "../config/abi/types";
import BEP20_ABI from "../config/abi/bep20.json";
import WETH_ABI from "../config/abi/weth.json";
import multiCallAbi from "../config/abi/Multicall.json";
import ERC20_BYTES32_ABI from "../config/abi/erc20_bytes32.json";
import ENS_PUBLIC_RESOLVER_ABI from "../config/abi/ens-public-resolver.json";
import ENS_ABI from "../config/abi/ens-registrar.json";
import { getMulticallAddress } from "../utils/addressHelpers";
import { ChainId, WETH } from "@disorn/golfswap-testnet-sdk";
import { EnsRegistrar } from "config/abi/types/EnsRegistrar";
// import { Erc20 } from 'config/abi/types/Erc20'

export const useERC20 = (address: string, withSignerIfPossible = true) => {
  const { library, account } = useActiveWeb3React();
  return useMemo(
    () =>
      getBep20Contract(
        address,
        withSignerIfPossible ? getProviderOrSigner(library, account) : null
      ),
    [account, address, library, withSignerIfPossible]
  );
};

export const useCake = () => {
  const { library } = useActiveWeb3React();
  return useMemo(() => getCakeContract(library.getSigner()), [library]);
};

export const useMasterchef = () => {
  const { library } = useActiveWeb3React();
  return useMemo(() => getMasterchefContract(library.getSigner()), [library]);
};

// returns null on errors
function useContract<T extends Contract = Contract>(
  address: string | undefined,
  ABI: any,
  withSignerIfPossible = true
): T | null {
  const { library, account } = useActiveWeb3React();

  return useMemo(() => {
    if (!address || !ABI || !library) return null;
    try {
      // console.log(getContract(address, ABI, withSignerIfPossible ? getProviderOrSigner(library, account) : null))
      return getContract(
        address,
        ABI,
        withSignerIfPossible ? getProviderOrSigner(library, account) : null
      );
    } catch (error) {
      console.error("Failed to get contract", error);
      return null;
    }
  }, [address, ABI, library, withSignerIfPossible, account]) as T;
}

export function useTokenContract(
  tokenAddress?: string,
  withSignerIfPossible?: boolean
) {
  return useContract<Bep20>(tokenAddress, BEP20_ABI, withSignerIfPossible);
}

export function useWETHContract(
  withSignerIfPossible?: boolean
): Contract | null {
  const { chainId } = useActiveWeb3React();
  return useContract<Weth>(
    chainId ? WETH[chainId].address : undefined,
    WETH_ABI,
    withSignerIfPossible
  );
}

export function useENSRegistrarContract(
  withSignerIfPossible?: boolean
): Contract | null {
  const { chainId } = useActiveWeb3React();
  let address: string | undefined;
  if (chainId) {
    // eslint-disable-next-line default-case
    switch (chainId) {
      case ChainId.MAINNET:
      case ChainId.TESTNET:
        address = "0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e";
        break;
    }
  }
  return useContract<EnsRegistrar>(address, ENS_ABI, withSignerIfPossible);
}

export function useENSResolverContract(
  address: string | undefined,
  withSignerIfPossible?: boolean
): Contract | null {
  return useContract<EnsPublicResolver>(
    address,
    ENS_PUBLIC_RESOLVER_ABI,
    withSignerIfPossible
  );
}

export function useMulticallContract() {
  return useContract<Multicall>(getMulticallAddress(), multiCallAbi, false);
}

export function useBytes32TokenContract(
  tokenAddress?: string,
  withSignerIfPossible?: boolean
): Contract | null {
  return useContract<Erc20Bytes32>(
    tokenAddress,
    ERC20_BYTES32_ABI,
    withSignerIfPossible
  );
}
