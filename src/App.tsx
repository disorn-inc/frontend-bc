import { lazy } from 'react';
import { ResetCSS } from '@pancakeswap/uikit';
import GlobalStyle from './style/Global';
import { Route, Router, Switch } from 'react-router-dom';
import history from './routerHistory'
import { useWeb3React } from '@web3-react/core';
import React from 'react';
import SuspenseWithChunkError from './components/SuspenseWithChunkError';
import PageLoader from 'components/Loader/PageLoader';
import Menu from 'components/Menu';
import Pools from './views/Pools'
import useUserAgent from 'hooks/useUserAgent';
import useSentryUser from 'hooks/useSentryUser';
import BigNumber from 'bignumber.js';
import useEagerConnect from 'hooks/useEagerConnect';
import { usePollBlockNumber } from 'state/block/hooks';
import { ToastListener } from 'contexts/ToastsContext';
import {
  RedirectDuplicateTokenIds,
  RedirectOldAddLiquidityPathStructure,
  RedirectToAddLiquidity,
} from './views/AddLiquidity/redirects';
import Swap from 'views/Swap';
import { usePollCoreFarmData } from 'state/farms/hooks';

// Route-based code splitting
// Only pool is included in the main bundle because of it's the most visited page
const Home = lazy(() => import('./views/Home'))
const Farms = lazy(() => import('./views/Farms'))
const AddLiquidity = lazy(() => import('./views/AddLiquidity'))
const Liquidity = lazy(() => import('./views/Pool'))

BigNumber.config({
  EXPONENTIAL_AT: 1000,
  DECIMAL_PLACES: 80,
})

const App: React.FC = () => {
  const { account } = useWeb3React()

  usePollBlockNumber()
  useEagerConnect()
  // usePollCoreFarmData()
  useUserAgent()
  useSentryUser()
  // console.log(account);
  return (
    <Router history={history}>
        <ResetCSS />
        <GlobalStyle />
        <Menu >
          <SuspenseWithChunkError fallback={<PageLoader />}>
          <Switch>
              <Route path="/" exact>
                <Home />
              </Route>
              <Route path="/farms">
                <Farms />
              </Route>
              <Route path="/pools">
               <Pools />
              </Route>

              <Route exact strict path="/swap" component={Swap} />
              <Route exact strict path="/liquidity" component={Liquidity} />
              <Route exact strict path="/create" component={RedirectToAddLiquidity} />
              <Route exact path="/add" component={AddLiquidity} />
              <Route exact path="/add/:currencyIdA" component={RedirectOldAddLiquidityPathStructure} />
              <Route exact path="/add/:currencyIdA/:currencyIdB" component={RedirectDuplicateTokenIds} />
              <Route exact path="/create" component={AddLiquidity} />
              <Route exact path="/create/:currencyIdA" component={RedirectOldAddLiquidityPathStructure} />
              <Route exact path="/create/:currencyIdA/:currencyIdB" component={RedirectDuplicateTokenIds} />

    
          </Switch>
          </SuspenseWithChunkError>
        </Menu>    
        <ToastListener />    
    </Router>
  );
}

export default React.memo(App);
