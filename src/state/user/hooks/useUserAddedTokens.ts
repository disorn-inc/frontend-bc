import { ChainId, Token } from "@disorn/golfswap-testnet-sdk";
import { useMemo } from "react";
import { useSelector } from "react-redux";
import { AppState } from "../..";
import useActiveWeb3React from "../../../hooks/useActiveWeb3React";
import { deserializeToken } from "./helpers";

export default function useUserAddedTokens(): Token[] {
  const { chainId } = useActiveWeb3React();
  const serializedTokensMap = useSelector<AppState, AppState["user"]["tokens"]>(
    ({ user: { tokens } }) => tokens
  );

  return useMemo(() => {
    if (!chainId) return [];
    return Object.values(serializedTokensMap?.[chainId as ChainId] ?? {}).map(
      deserializeToken
    );
  }, [serializedTokensMap, chainId]);
}
