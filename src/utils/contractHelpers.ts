import { ethers } from 'ethers';

// Addresses
import {
    getAddress,
    // getAddress,
    getMasterChefAddress,
    getMulticallAddress,
} from './addressHelpers'

// ABI
import masterChefABI from '../config/abi/abiRaw/masterchefWeb.json'
import MultiCallAbi from '../config/abi/Multicall.json'
import bep20Abi from '../config/abi/bep20.json'
import mainAbi from '../config/abi/mainToken.json'
import { simpleRpcProvider } from './providers';

// Types
import {
    Bep20,
    MainToken,
    Multicall,
    MasterChef,
    Erc20,
} from '../config/abi/types'
import tokens from '../config/constants/tokens';
import { PoolCategory } from 'config/constants/types';
import { poolsConfig } from 'config/constants';

const getContract = (abi: any, address: string, signer?: ethers.Signer | ethers.providers.Provider) => {
    const signerOrProvider = signer ?? simpleRpcProvider
    // console.log(new ethers.Contract(address, abi, signerOrProvider))
    try {
        return new ethers.Contract(address, abi, signerOrProvider)
    } catch (error) {
        console.log(error);
    }
    
}

// export const getSouschefContract = (id: number, signer?: ethers.Signer | ethers.providers.Provider) => {
//     const config = poolsConfig.find((pool) => pool.sousId === id)
//     const abi = config.poolCategory === sousChef
//     return getContract(abi, getAddress(config.contractAddress), signer) as SousChef
// }

export const getBep20Contract = (address: string, signer?: ethers.Signer | ethers.providers.Provider) => {
    return getContract(bep20Abi, address, signer) as Bep20
}

export const getCakeContract = (signer?: ethers.Signer | ethers.providers.Provider) => {
    return getContract(mainAbi, tokens.cake.address, signer) as MainToken
}

export const getMulticallContract = (signer?: ethers.Signer | ethers.providers.Provider) => {
    return getContract(MultiCallAbi, getMulticallAddress(), signer) as Multicall
}

export const getMasterchefContract = (signer?: ethers.Signer | ethers.providers.Provider) => {
    return getContract(masterChefABI, getMasterChefAddress(), signer) as MasterChef
}