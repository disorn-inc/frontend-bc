import { Link, Text } from "@pancakeswap/uikit"
import { useTranslation } from "../../contexts/Localization"
import useActiveWeb3React from "../../hooks/useActiveWeb3React"
import { getBscScanLink } from "../../utils"
import truncateHash from "../../utils/truncateHash"

interface DescriptionWithTxProps {
    description?: string
    txHash?: string
  }
  
const DescriptionWithTx: React.FC<DescriptionWithTxProps> = ({ txHash, children }) => {
    const { chainId } = useActiveWeb3React()
    const { t } = useTranslation()
  
    return (
      <>
        {typeof children === 'string' ? <Text>{children}</Text> : children}
        {txHash && (
          <Link href={getBscScanLink(txHash, 'transaction', chainId)}>
            {t('View on BscScan')}: {truncateHash(txHash, 8, 0)}
          </Link>
        )}
      </>
    )
}
  
export default DescriptionWithTx