import { useTranslation } from "contexts/Localization"
import { useCakeBusdPrice } from "hooks/useBUSDPrice"
import useTheme from "hooks/useTheme"
import { Menu as UikitMenu } from '@pancakeswap/uikit'
import { Link, useLocation } from "react-router-dom"
// import { usePhishingBannerManager } from "state/user/hooks"
import config from "./config/config"
import GlobalSettings from "./GlobalSettings"
import { getActiveMenuItem, getActiveSubMenuItem } from "./utils"
import { languageList } from "config/localization/languages"
// import PhishingWarningBanner from "components/PhishingWarningBanner"
import { footerLinks } from "./config/footerConfig"
import UserMenu from "./UserMenu"

const Menu = (props) => {
    const { isDark, toggleTheme } = useTheme()
    const cakePriceUsd = useCakeBusdPrice()
    const { currentLanguage, setLanguage, t } = useTranslation()
    const { pathname } = useLocation()
    // const [showPhishingWarningBanner] = usePhishingBannerManager()
  
    const activeMenuItem = getActiveMenuItem({ menuConfig: config(t), pathname })
    const activeSubMenuItem = getActiveSubMenuItem({ menuItem: activeMenuItem, pathname })
  
    return (
      <UikitMenu
        linkComponent={({ href, ...linkProps }: any) => {
          return <Link to={href} {...linkProps} />
        }}
        userMenu={<UserMenu />}
        globalMenu={<GlobalSettings />}
        // banner={showPhishingWarningBanner && <PhishingWarningBanner />}
        isDark={isDark}
        toggleTheme={toggleTheme}
        currentLang={currentLanguage.code}
        langs={languageList}
        setLang={setLanguage}
        cakePriceUsd={cakePriceUsd}
        links={config(t)}
        subLinks={activeMenuItem?.hideSubNav ? [] : activeMenuItem?.items}
        footerLinks={footerLinks(t)}
        activeItem={activeMenuItem?.href}
        activeSubItem={activeSubMenuItem?.href}
        buyCakeLabel={t('Buy CAKE')}
        {...props}
      />
    )
  }
  
  export default Menu