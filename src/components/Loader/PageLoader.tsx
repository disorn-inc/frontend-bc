import { Spinner } from "@pancakeswap/uikit"
import Page from "components/Layout/Page"
import styled from "styled-components"

const Wrapper = styled(Page)`
  display: flex;
  justify-content: center;
  align-items: center;
`

const PageLoader: React.FC = () => {
  return (
    <Wrapper>
      <Spinner />
    </Wrapper>
  )
}

export default PageLoader