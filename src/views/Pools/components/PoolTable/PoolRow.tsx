import { useMatchBreakpoints } from "@pancakeswap/uikit"
import useDelayedUnmount from "hooks/useDelayedUnmount"
import { useState } from "react"
import { DeserializedPool } from "state/types"
import styled from "styled-components"
import ActionPanel from "./ActionPanel/ActionPanel"
import AprCell from "./Cells/AprCell"
import EarningsCell from "./Cells/EarningsCell"
import EndsInCell from "./Cells/EndsInCell"
import ExpandActionCell from "./Cells/ExpandActionCell"
import NameCell from "./Cells/NameCell"
import StakedCell from "./Cells/StakedCell"
import TotalStakedCell from "./Cells/TotalStakedCell"

interface PoolRowProps {
  pool: DeserializedPool
  account: string
  userDataLoaded: boolean
}

const StyledRow = styled.div`
  background-color: transparent;
  display: flex;
  cursor: pointer;
`

const PoolRow: React.FC<PoolRowProps> = ({ pool, account, userDataLoaded }) => {
  const { isXs, isSm, isMd, isLg, isXl, isXxl, isTablet, isDesktop } = useMatchBreakpoints()
  const isLargerScreen = isLg || isXl || isXxl
  const isXLargerScreen = isXl || isXxl
  const [expanded, setExpanded] = useState(false)
  const shouldRenderActionPanel = useDelayedUnmount(expanded, 300)

  const toggleExpanded = () => {
    setExpanded((prev) => !prev)
  }

  const isCakePool = pool.sousId === 0

  return (
    <>
      <StyledRow role="row" onClick={toggleExpanded}>
        <NameCell pool={pool} />
        {/* {pool.vaultKey ? (
          ((isXLargerScreen && pool.vaultKey === VaultKey.IfoPool) || pool.vaultKey === VaultKey.CakeVault) && (
            <AutoEarningsCell pool={pool} account={account} />
          )
        ) : (
          <EarningsCell pool={pool} account={account} userDataLoaded={userDataLoaded} />
        )} */}
        {(
          <EarningsCell pool={pool} account={account} userDataLoaded={userDataLoaded} />
        )}
        {/* {pool.vaultKey === VaultKey.IfoPool ? (
          <IFOCreditCell account={account} />
        ) : isXLargerScreen && isCakePool ? (
          <StakedCell pool={pool} account={account} userDataLoaded={userDataLoaded} />
        ) : null} */}
        {isXLargerScreen && isCakePool ? (
          <StakedCell pool={pool} account={account} userDataLoaded={userDataLoaded} />
        ) : null}
        {isLargerScreen && !isCakePool && <TotalStakedCell pool={pool}/>}
        {/* {pool.vaultKey ? <AutoAprCell pool={pool} /> : <AprCell pool={pool} />} */}
        {<AprCell pool={pool} />}
        {isLargerScreen && isCakePool && <TotalStakedCell pool={pool} />}
        {isDesktop && !isCakePool && <EndsInCell pool={pool} />}
        <ExpandActionCell expanded={expanded} isFullLayout={isTablet || isDesktop} />
      </StyledRow>
      {shouldRenderActionPanel && (
        <ActionPanel
          account={account}
          pool={pool}
          userDataLoaded={userDataLoaded}
          expanded={expanded}
          breakpoints={{ isXs, isSm, isMd, isLg, isXl, isXxl }}
        />
      )}
    </>
  )
}

export default PoolRow