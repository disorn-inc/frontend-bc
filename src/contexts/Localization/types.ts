import { Language } from "@pancakeswap/uikit";
import { ReactText } from "react";

export type ContextData = {
    [key: string]: ReactText | undefined
}

export interface ProviderState{
    isFetching: boolean
    currentLanguage: Language
}

export interface ContextApi extends ProviderState {
    setLanguage: (language: Language | undefined) => void
    t: Translate
  }
  
  export type Translate = (key: string, data?: ContextData) => string