import { ChainId, Token } from "@disorn/golfswap-testnet-sdk";
import { serializeToken } from "../../state/user/hooks/helpers";
import { SerializedToken } from "./types";

const { MAINNET, TESTNET } = ChainId;

interface TokenList {
  [symbol: string]: Token;
}

const defineTokens = <T extends TokenList>(t: T) => t;

export const mainnetTokens = defineTokens({
  wbnb: new Token(
    TESTNET,
    "0x094616F0BdFB0b526bD735Bf66Eca0Ad254ca81F",
    18,
    "WBNB",
    "Wrapped BNB",
    "https://www.binance.com/"
  ),
  cake: new Token(
    TESTNET,
    "0xbbae870d02dbedac0ea683c905ef7ee254d70800",
    18,
    "MAIN",
    "MAIN Token",
    "https://pancakeswap.finance/"
  ),
  syrup: new Token(
    TESTNET,
    "0x1f322998551c198126001cc8485ef2a9d3e3f114",
    18,
    "EARN",
    "EARN Token",
    "https://pancakeswap.finance/"
  ),
  busd: new Token(
    TESTNET,
    "0xeD24FC36d5Ee211Ea25A80239Fb8C4Cfd80f12Ee",
    18,
    "BUSD",
    "Binance USD",
    "https://www.paxos.com/busd/"
  ),
  safemoon: new Token(
    TESTNET,
    "0x1f322998551c198126001cc8485ef2a9d3e3f114",
    18,
    "safe",
    "Binance USD",
    "https://www.paxos.com/busd/"
  ),
  bondly: new Token(
    TESTNET,
    "0x1f322998551c198126001cc8485ef2a9d3e3f114",
    18,
    "safe",
    "Binance USD",
    "https://www.paxos.com/busd/"
  ),
} as const);

export const testnetTokens = defineTokens({
  wbnb: new Token(
    TESTNET,
    "0x094616F0BdFB0b526bD735Bf66Eca0Ad254ca81F",
    18,
    "WBNB",
    "Wrapped BNB",
    "https://www.binance.com/"
  ),
  cake: new Token(
    TESTNET,
    "0xbbae870d02dbedac0ea683c905ef7ee254d70800",
    18,
    "MAIN",
    "MAIN Token",
    "https://pancakeswap.finance/"
  ),
  syrup: new Token(
    TESTNET,
    "0x1f322998551c198126001cc8485ef2a9d3e3f114",
    18,
    "EARN",
    "EARN Token",
    "https://pancakeswap.finance/"
  ),
  busd: new Token(
    TESTNET,
    "0xeD24FC36d5Ee211Ea25A80239Fb8C4Cfd80f12Ee",
    18,
    "BUSD",
    "Binance USD",
    "https://www.paxos.com/busd/"
  ),
} as const);

const tokens = () => {
  const chainId = process.env.REACT_APP_CHAIN_ID;

  // If testnet - return list comprised of testnetTokens wherever they exist, and mainnetTokens where they don't
  if (parseInt(chainId, 10) === ChainId.TESTNET) {
    return Object.keys(mainnetTokens).reduce((accum, key) => {
      return { ...accum, [key]: testnetTokens[key] || mainnetTokens[key] };
    }, {} as typeof testnetTokens & typeof mainnetTokens);
  }

  return mainnetTokens;
};

const unserializedTokens = tokens();

type SerializedTokenList = Record<
  keyof typeof unserializedTokens,
  SerializedToken
>;

export const serializeTokens = () => {
  const serializedTokens = Object.keys(unserializedTokens).reduce(
    (accum, key) => {
      return { ...accum, [key]: serializeToken(unserializedTokens[key]) };
    },
    {} as SerializedTokenList
  );

  return serializedTokens;
};

export default unserializedTokens;
