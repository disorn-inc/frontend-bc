import { PoolCategory, SerializedPoolConfig } from "./types";
import { serializeTokens } from "./tokens"; 

const serializedTokens = serializeTokens()

const pools: SerializedPoolConfig[] = [
    {
      sousId: 0,
      stakingToken: serializedTokens.cake,
      earningToken: serializedTokens.cake,
      contractAddress: {
        97: '0x4BFb145C5aEf4D5C2eb604b23cc81446948447f9',
        56: '',
      },
      poolCategory: PoolCategory.CORE,
      harvest: true,
      tokenPerBlock: '3',
      sortOrder: 1,
      isFinished: false,
    }
]

export default pools