import { serializeTokens } from "./tokens"
import { SerializedFarmConfig } from "./types"

const serializedTokens = serializeTokens()

const farms: SerializedFarmConfig[] = [
    /**
     * These 3 farms (PID 0, 251, 252) should always be at the top of the file.
     */
    {
      pid: 0,
      lpSymbol: 'CAKE',
      lpAddresses: {
        97: '0xbbae870d02dbedac0ea683c905ef7ee254d70800',
        56: '',
      },
      token: serializedTokens.syrup,
      quoteToken: serializedTokens.wbnb,
    },
    {
      pid: 1,
      lpSymbol: 'MAIN-BNB LP',
      lpAddresses: {
        97: '0x7d0e350aa1e9a6d8e1c5646306b57af7ab6a626f',
        56: '',
      },
      token: serializedTokens.cake,
      quoteToken: serializedTokens.wbnb,
    },
    {
      pid: 2,
      lpSymbol: 'MAIN-BUSD LP',
      lpAddresses: {
        97: '0x778F2387a79F60423189024EC0aa912e0C31096A',
        56: '',
      },
      token: serializedTokens.cake,
      quoteToken: serializedTokens.busd,
  },
  // {
  //   pid: 3,
  //   lpSymbol: 'MAIN-BNB LP',
  //   lpAddresses: {
  //     97: '0x089f0460e7e8132fa0acf5bf7ec4cba2de7c7bb0',
  //     56: '',
  //   },
  //   token: serializedTokens.cake,
  //   quoteToken: serializedTokens.wbnb,
  // },
]

export default farms